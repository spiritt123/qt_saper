#include "cell.h"

void Cell::setStatus(int stat)
{
    status_ = stat;
}

int Cell::getStatus()
{
    return status_;
}

void Cell::plusStatus()
{
    ++status_;
}

void Cell::mousePressEvent(QMouseEvent *e)
{
    if (e->button() == Qt::RightButton)
    {
        emit rightClicked();
    }
    if (e->button() == Qt::LeftButton)
    {
        emit leftClicked();
    }
}

void Cell::mouseDoubleClickEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton)
    {
        emit leftDoubleClicked();
    }
}
