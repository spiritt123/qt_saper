#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "field.h"
#include "settingswidget.h"
#include <QMainWindow>
#include <QTimer>
#include <QTime>


namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Field *field_;
    QTimer *timer;
    QTime time_start_game_;

    SettingsWidget *settings_widget_;
    int height_in_cells_, width_in_cells_, all_mines_;

protected:
    void resizeEvent(QResizeEvent *event) override;

public slots:
    void updateMineCounter();
    void tryStartTimer();
private slots:
    void updateTime();
    void newGame();
    void openSettings();
    void closeSettings();
    void acceptSettings();
};

#endif // MAINWINDOW_H
