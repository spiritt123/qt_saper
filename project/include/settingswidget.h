#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QRadioButton>
#include <QPushButton>
#include <QSpinBox>
#include <QLabel>

class SettingsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsWidget(QWidget *parent = nullptr, QWidget *main_window = nullptr);
    void getSettings(int &height_in_cells, int &width_in_cells, int &all_mines, QString &name_mode);
    ~SettingsWidget();

private:
    QWidget *main_window_;
    QGridLayout *settings_layout_;
    QRadioButton *easy_mode_, *normal_mode_, *hard_mode_, *user_mode_;
    QPushButton *accept_settings_, *cancel_settings_;
    QSpinBox *height_, *width_, *mines_;
    QLabel *height_label_, *width_label_, *mines_label_;
    QString name_mode_;

signals:
    void closed();
private slots:
    void setEasy();
    void setNormal();
    void setHard();
    void setUser();
    void closeEvent(QCloseEvent *event);

};

#endif // SETTINGSWIDGET_H
